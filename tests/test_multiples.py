# -- coding: UTF-8 --
# test_multiples.py

import pytest
from ..multiples import multiples

def test_multiples():
    assert multiples.processNumber(7) == '7'
    assert multiples.processNumber(9) == 'Three'
    assert multiples.processNumber(10) == 'Five'
    assert multiples.processNumber(15) == 'ThreeFive'
    assert multiples.processNumber(29) == '29'
    assert multiples.processNumber(30) == 'ThreeFive'
    assert multiples.processNumber(64) == '64'
    assert multiples.processNumber(71) == '71'
    assert multiples.processNumber(82) == '82'
    assert multiples.processNumber(83) == '83'
    assert multiples.processNumber(84) == 'Three'
    assert multiples.processNumber(85) == 'Five'
    assert multiples.processNumber(90) == 'ThreeFive'