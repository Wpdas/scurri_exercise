# -- coding: UTF-8 --
# test_postcoder.py

"""
Tests are performed using the pytest package. To do this, run py.test 
in the root directory of the project.
"""

import pytest
from ..postalcode import UKPostalCode

uk_postalcode_instance = UKPostalCode()

def test_validation_postcodes():
    # Success cases
    assert uk_postalcode_instance.validate('BS98 1TL') == True # TV Licensing[44]
    assert uk_postalcode_instance.validate('IV21 2LR') == True # Two Lochs Radio
    assert uk_postalcode_instance.validate('DA1 1RT') == True # Dartford F.C. (nicknamed The Darts)
    assert uk_postalcode_instance.validate('DE99 3GG') == True # Egg Banking
    assert uk_postalcode_instance.validate('DE55 4SW') == True # Slimming World
    assert uk_postalcode_instance.validate('DH98 1BT') == True # British Telecom
    assert uk_postalcode_instance.validate('DH99 1NS') == True # National Savings certificates administration
    assert uk_postalcode_instance.validate('E14 5HQ') == True # HSBC headquarters
    assert uk_postalcode_instance.validate('E16 1XL') == True # ExCeL London[49]
    assert uk_postalcode_instance.validate('E20 2AQ') == True # Olympic Aquatics Centre

    # Error cases
    assert uk_postalcode_instance.validate('WR16AE') == False # No space separating inward and outward code
    assert uk_postalcode_instance.validate('AB1 1AA') == False  # AB is a double digit only district
    assert uk_postalcode_instance.validate('SW1W 0!T') == False # Invalid character in inward code
    assert uk_postalcode_instance.validate('W11 2X') == False  # Invalid inward code length
    assert uk_postalcode_instance.validate('W1 A 2X') == False  # Invalid format
    assert uk_postalcode_instance.validate('wr13 6ae') == False    # Lowercase
    assert uk_postalcode_instance.validate('WA1A 2ABR') == False    # Too long
    assert uk_postalcode_instance.validate('BR21 1AA') == False    # BR is a single-digit district only
    assert uk_postalcode_instance.validate('AB2 6BS') == False    # AB is a double-digit district only
    assert uk_postalcode_instance.validate('HP0 8YH') == False    # Areas with a district '0' (zero): BL, BS, CM, CR, FY, HA, PR, SL, SS
    assert uk_postalcode_instance.validate('GH1W 7YY') == False    # Only zones with digit at end of outward code are: EC1–EC4 (but not EC50), SW1, W1, WC1, WC2, and part of E1 (E1W), N1 (N1C and N1P), NW1 (NW1W) and SE1 (SE1P).
    assert uk_postalcode_instance.validate('QK8 9AC') == False    # The letters QVX are not used in the first position.
    assert uk_postalcode_instance.validate('WI9 8BA') == False    # The letters IJZ are not used in the second position.
    assert uk_postalcode_instance.validate('A1I 8GH') == False    # The only letters to appear in the third position are ABCDEFGHJKPSTUW when the structure starts with A9A.
    assert uk_postalcode_instance.validate('AW1O 6AX') == False    # The only letters to appear in the fourth position are ABEHMNPRVWXY when the structure starts with AA9A.
    assert uk_postalcode_instance.validate('W1 2AV') == False    # The final two letters do not use the letters CIKMOV, so as not to resemble digits or each other when hand-written.
    assert uk_postalcode_instance.validate('DS1 DBH') == False    # Post code sectors are one of ten digits: 0 to 9