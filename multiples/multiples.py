# -- coding: UTF-8 --
# multiples.py

def processNumber(number):
    """
    Return a string with information about the number.
    If number is multiples of 3, return "Three".
    If number is multiples of 5, return "Five".
    If number is multiples of 3 and 5, return "ThreeFive".
    However, if the number is not a multiple of 3 or 5, it returns only the number.
    """

    label = ''
    if number % 3 == 0:
        label = 'Three'
    if number % 5 == 0:
        label += 'Five'
    return label or str(number)

# Print processed numbers from 1 to 100 only when this script is run.
if __name__ == "__main__":
  print('\n'.join(processNumber(number) for number in range(1,101)))